**NOTICE: this repository contains a proof-of-concept (PoC) and is still a work in progress (WIP)**

# sar-preinit

_SharkBaitOS's preinit for System-As-Root devices._

## Usage

1. Put your Android device's `boot.img` inside artifacts and rename it to `ogboot.img`
2. Then patch the image:
	```bash
	$ make all
	```
3. Flash the `boot_mod.img` to circumvent System-As-Root.

## License

SPDX-License-Identifier: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

## Reference

- [Booting Gentoo from SAR-Preinit; or Debugging the init, the Hard Way](https://wantguns.gitlab.io/blog/sharkbait/booting_gentoo_using_preinit)
