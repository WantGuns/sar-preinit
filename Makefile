RAMDISKDIR		= ramdisk
OUTDIR			= out
TOOLSDIR		= tools
ARTIFACTSDIR	= artifacts
CHUTEDIR		= $(OUTDIR)/chute
UNPACKDIR		= $(OUTDIR)/unpack

RAMDISKUNPACK	= $(UNPACKDIR)/initrd.img
BOOTIMGCFG		= $(UNPACKDIR)/bootimg.cfg
KERNELUNPACK	= $(UNPACKDIR)/zImage

RAMDISKOUT		= $(OUTDIR)/ramdisk_mod.cpio
RAMDISK			= $(OUTDIR)/ramdisk.cpio
BOOTOUT			= $(OUTDIR)/boot_mod.img

RAMDISKCHUTE	= $(CHUTEDIR)/ramdisk.cpio
KERNELCHUTE		= $(CHUTEDIR)/kernel
BOOTCHUTE		= $(CHUTEDIR)/boot.img
NEW_BOOT		= $(CHUTEDIR)/new-boot.img

OGBOOT			= $(ARTIFACTSDIR)/ogboot.img
BOOT			= $(ARTIFACTSDIR)/boot.img

CPIO 			= cpio
MAGISKBOOT		= $(TOOLSDIR)/magiskboot
ABOOTIMG		= $(TOOLSDIR)/abootimg

all: clean bootimg_chute

clean:
	rm -rf $(OUTDIR)

cleanchute:
	rm -rf $(CHUTEDIR)

$(OUTDIR):
	mkdir $(OUTDIR)

$(RAMDISK): $(RAMDISKDIR) $(OUTDIR)
	cd $(RAMDISKDIR) && \
	find . | $(CPIO) -o -H newc > $(abspath $(RAMDISKOUT))

$(CHUTEDIR): $(OUTDIR) $(MAGISKBOOT) $(OGBOOT)
	mkdir $(CHUTEDIR)
	cp $(OGBOOT) $(BOOTCHUTE)
	cd $(CHUTEDIR) && \
		$(abspath $(MAGISKBOOT)) unpack $(abspath $(BOOTCHUTE))

$(UNPACKDIR): $(OUTDIR) $(ABOOTIMG)	$(BOOT)
	mkdir -p $(UNPACKDIR)

bootimg_chute: $(CHUTEDIR) $(MAGISKBOOT) $(RAMDISK)
	cd $(CHUTEDIR) && \
		rm -rf ramdisk.cpio
	cp $(RAMDISKOUT) $(RAMDISKCHUTE)
	## Start Patching
	# 
	# skip_initramfs -> want_initramfs
	#
	-	cd $(CHUTEDIR) && \
		$(abspath $(MAGISKBOOT)) hexpatch $(abspath $(KERNELCHUTE)) \
		736B69705F696E697472616D6673 \
		77616E745F696E697472616D6673
	#
	# Remove Samsung RKP
	#
	-	cd $(CHUTDIR) && \
		$(abspath $(MAGISKBOOT)) hexpatch $(abspath $(KERNELCHUTE)) \
  		49010054011440B93FA00F71E9000054010840B93FA00F7189000054001840B91FA00F7188010054 \
  		A1020054011440B93FA00F7140020054010840B93FA00F71E0010054001840B91FA00F7181010054
	#
	# Remove Samsung defex
  	# Before: [mov w2, #-221]   (-__NR_execve)
  	# After:  [mov w2, #-32768]
	#
	-	cd $(CHUTDIR) && \
		$(abspath $(MAGISKBOOT)) hexpatch $(abspath $(KERNELCHUTE)) \
		821B8012 \
		E2FF8F12
	## Done with the Patches
	cd $(CHUTEDIR) && \
		$(abspath $(MAGISKBOOT)) repack $(abspath $(BOOTCHUTE))
	cp $(NEW_BOOT) $(BOOTOUT)

unpack: $(UNPACKDIR)
	cd $(UNPACKDIR) && \
		$(abspath $(ABOOTIMG)) -x $(abspath $(BOOT))

repack: $(UNPACKDIR) $(BOOTIMGCFG) $(RAMDISKUNPACK) $(KERNELUNPACK)
	cd $(ARTIFACTSDIR) && \
		$(abspath $(ABOOTIMG)) --create $(abspath $(OGBOOT)) \
			-f $(abspath $(BOOTIMGCFG))	\
			-k $(abspath $(KERNELUNPACK)) \
			-r $(abspath $(RAMDISKUNPACK)) 
	# Starting the patching subroutine
	$(MAKE) bootimg_chute
